import 'package:flutter/material.dart';
import 'package:locucionar/src/MyApp.dart';
import 'package:flutter/services.dart';
 
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}