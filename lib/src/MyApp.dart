import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:locucionar/src/pages/Carrusel.dart';
import 'package:locucionar/src/pages/Login.dart';
import 'package:locucionar/src/pages/Principal.dart';
import 'package:locucionar/src/pages/RecuperarPassword.dart';
import 'package:locucionar/src/pages/SplashScreen.dart';
import 'package:locucionar/src/pages/WebViewPaypal.dart';
import 'package:locucionar/src/pages/WebViewMercadoPago.dart';
import 'package:locucionar/src/provider/PushNotificationsProvider.dart';
import 'package:locucionar/src/pages/Notificaciones.dart';
import 'package:locucionar/src/pages/Notificacion.dart';

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class MyApp extends StatefulWidget{

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  //Para las notificaciones locales
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();

    initializing();

    //Para las notificaciones push desde firebase
    final pushProvider = new PushNotificationProvider();
    pushProvider.initNotifications();

    pushProvider.mensajes.listen(( data ){
      //Aca es donde manejamos que se hace con las notificaciones que se reciben...

      Map<String, dynamic> info = json.decode(data);

      //Si la aplicacion esta abierta
      if(info['tipoNotificacion']=="onMessage" && info['plataforma']=="Android"){
        String datos = info['data']['title'].toString()+"|"+info['data']['body'].toString();
        _showNotifications(datos);
      }
      else{
        String payload = info['data']['title'].toString()+"|"+info['data']['body'].toString();
        navigatorKey.currentState.pushNamed('/Notificacion',arguments: payload);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'LocucionAr',
      initialRoute: "/",
      routes: {
        "/Carrusel" : (BuildContext context) =>Carrusel(text:""),
        "/Login" : (BuildContext context) => Login(),
        "/Principal" : (BuildContext context) => Principal(),
        "/" : (BuildContext context) => SplashScreen(),
        "/Notificaciones" : (BuildContext context) => Notificaciones(),
        "/Notificacion" : (BuildContext context) => Notificacion("",""),
        "/PagoPaypal" : (BuildContext context) => WebViewPaypal(),
        "/PagoMercadoPago" : (BuildContext context) => WebViewMercadoPago(),
        "/RecuperarPassword" : (BuildContext context) => RecuperarPassword(),
      }
    
    );
  }

  void initializing() async{
    androidInitializationSettings = AndroidInitializationSettings('app_icon'); 
    iosInitializationSettings = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,onSelectNotification: onSelectNotification);
  }

  void _showNotifications(String info) async {
    await notification(info);
  }

  Future<void> notification(String info) async{
    AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
      'Channel ID', 
      'Channel title', 
      'channel body',
      priority: Priority.High,
      importance: Importance.Max,
      /*ticker: 'test'*/);

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails = NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    
    String payl = info;
    var corte = info.indexOf("|");
    var titulo = info.substring(0,corte);
    var cuerpo = info.substring(corte+1,info.length);

    await flutterLocalNotificationsPlugin.show(0, titulo, cuerpo, notificationDetails, payload: payl);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
        navigatorKey.currentState.pushNamedAndRemoveUntil('/Notificacion',ModalRoute.withName('/Principal'),arguments: payload);
    }
  }

  //Creo que esto es para que muestre la notificacion en IOS
  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async{
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
          isDefaultAction: true,
          onPressed: () {
            print("");
          },
          child: Text('Ok'),
          ),
      ],
    );
  }
}