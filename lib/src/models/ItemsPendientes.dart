class Items{
  List<ItemPendiente> itemsPendientes = new List();
  Items.fromJsonList( List<dynamic> jsonList){
    if (jsonList == null) return;

    jsonList.forEach( (item) {
      final itemPendiente = ItemPendiente.fromJsonMap(item);
      itemsPendientes.add(itemPendiente);
    });
  }
}

class ItemPendiente{
  int id;
  String fecha;
  String concepto;
  String importe;
  String importeNumero;

  ItemPendiente({
    this.id,
    this.fecha,
    this.concepto,
    this.importe,
    this.importeNumero,
  });

  ItemPendiente.fromJsonMap(Map<String, dynamic> json){
    id = json['id'];
    fecha = json['fecha'];
    concepto = json['concepto'];
    importe = json['importe'];
    importeNumero = json['importe_numero'];
  }
}