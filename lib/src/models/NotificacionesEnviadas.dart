class NotificacionesEnviadas{
  List<NotificacionEnviada> notificacionesEnviadas = new List();
  NotificacionesEnviadas.fromJsonList( List<dynamic> jsonList){
    if (jsonList == null) return;

    jsonList.forEach( (item) {
      final notificacion = NotificacionEnviada.fromJsonMap(item);
      notificacionesEnviadas.add(notificacion);
    });
  }
}

class NotificacionEnviada{
  String idNotificacion;
  String titulo;
  String cuerpo;

  NotificacionEnviada({
    this.idNotificacion,
    this.titulo,
    this.cuerpo,
  });

  NotificacionEnviada.fromJsonMap(Map<String, dynamic> json){
    idNotificacion  = json['id_notificacion'];
    titulo          = json['titulo'];
    cuerpo          = json['cuerpo'];
  }
}