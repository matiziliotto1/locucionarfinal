import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PestaniaPresentacion extends StatelessWidget {

  final String imagen;
  final String titulo;
  final String texto;

  PestaniaPresentacion({@required this.imagen,this.titulo,this.texto});


  @override
  Widget build(BuildContext context) {
    final double height=MediaQuery.of(context).size.height;
    final double width=MediaQuery.of(context).size.width;


    return Container(
      child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                //height: height * 0.4,
                //margin:EdgeInsets.only(top:height * 0.1),
                child: CachedNetworkImage(
                  imageUrl: "https://clientes.locucionar.com/app_clientes/locucionar_app/imagenes/"+this.imagen,
                  fit: BoxFit.cover,
                  placeholder: (context,url) => Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Color.fromRGBO(40, 56, 73, 1)
                        ),
                      ),
                    )
                  ),
                  errorWidget: (context,url,error) => Icon(Icons.error),
                  fadeInCurve: Curves.easeIn,
                )
              ),
              Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                        "assets/images/logo_blanco.png",
                        scale: 3.5,
                    ),
                  ],
                ),
              ),
              Container(
                margin:EdgeInsets.only(top:height * 0.6,left: width * 0.05,right: width * 0.05),
                child:
                Column(
                  children: <Widget>[
                    Text(
                      this.titulo,
                      style: TextStyle(
                        fontSize: width * 0.06,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      this.texto,
                      style: TextStyle(
                        fontSize: width * 0.045,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )
              )
            ],
              
      ),
    );
  }
}