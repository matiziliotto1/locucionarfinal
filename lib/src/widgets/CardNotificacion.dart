import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
// import 'package:locucionar/src/pages/Notificacion.dart';

import 'package:locucionar/utils/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

class CardNotificacion extends StatefulWidget{

  String idNotificacion;
  String titulo;
  String cuerpo;

  CardNotificacion(this.idNotificacion,this.titulo,this.cuerpo);

  @override
  _CardNotificacionState createState() => _CardNotificacionState(idNotificacion,titulo,cuerpo);
}

class _CardNotificacionState extends State<CardNotificacion> {
  bool _eliminada = false;

  String idNotificacion;
  String titulo;
  String cuerpo;
  _CardNotificacionState(this.idNotificacion,this.titulo,this.cuerpo);

  @override
  Widget build(BuildContext context) {
    return _eliminada == false ?
      Padding(
        padding: EdgeInsets.only(left:8.0,right: 8.0,bottom: 4.0,top:4.0),
        child: Card(
          elevation: 5.0,
          child: InkWell(
            splashColor: Color.fromRGBO(62, 111, 163, 1),
            onTap: (){},
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    color: Color.fromRGBO(40, 56, 73, 1),
                    width: 2.5,
                  ),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text(
                      titulo,
                      style: TextStyle(
                        color: globals.infTextColor,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    subtitle: idNotificacion!="-1" ? Text(
                      cuerpo,
                      style: TextStyle(
                        color: globals.itemsTextColor,
                      ),
                    ):null,
                  ),

                  idNotificacion!="-1" ?
                  ButtonBar(
                    
                    alignment: MainAxisAlignment.center,
                    children: <Widget>[
                      /*
                      FlatButton.icon(
                        onPressed: (){
                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => Notificacion(widget.titulo,widget.cuerpo)));
                        },
                        icon: Icon(
                          Icons.remove_red_eye,
                          color:Color.fromRGBO(41, 171, 226, 1),
                        ),
                        label: Text(
                          "Ver",
                          style: TextStyle(
                            color:Color.fromRGBO(41, 171, 226, 1),
                          ),
                        )
                      ),
                      */
                      FlatButton.icon(
                        onPressed: (){
                          showDialog(
                            context: context,
                            child: AlertDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              title: Text('Estas seguro de eliminar esta notificación?'),
                              actions: <Widget>[
                                RaisedButton(
                                  onPressed: () async {
                                    //Elimino la notificacion de la tablas relacionadas
                                    SharedPreferences sharedPreferences;
                                    sharedPreferences = await SharedPreferences.getInstance();

                                    var idUsuario = sharedPreferences.getInt('id_cliente').toString();
                                    var url = '';
                                    Map data;

                                    url = 'https://clientes.locucionar.com/app_clientes/API/deleteNotificacion.php';
                                    data= {
                                      'id_notificacion': idNotificacion,
                                      'id_usuario': idUsuario,
                                    };
                                    await http.post(url, body:data);
                                    setState(() {
                                      _eliminada = true;
                                    });
                                    Navigator.of(context).pop(false);
                                    
                                  },
                                  child: Text(
                                    'Si',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: Color.fromRGBO(41, 171, 226, 1),
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                  child: Text('No'),
                                  color: Colors.red.shade400,
                                ),
                              ],
                            ),
                          ) ??
                          false;
                        },
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red.shade400,
                        ),
                        label: Text(
                          "Eliminar",
                          style: TextStyle(
                            color:Colors.red.shade400
                          ),
                        ),
                      ),
                    ],
                  )
                  : Container(),
                ],
              ),
            ),
          ),
        ),
      ) : Container();
  }
}