import 'package:flutter/material.dart';

import 'package:locucionar/utils/globals.dart' as globals;

class CustomListTitle extends StatelessWidget{

  IconData icon ;
  String text;
  Function onTap;

  CustomListTitle(this.icon,this.text,this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom:BorderSide(
              color: Colors.grey.shade300,
            )
          )
        ),
        child: InkWell(
          splashColor: Color.fromRGBO(62, 111, 163, 1),
          onTap: onTap,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    this.icon == Icons.notifications_none && globals.notificaciones? 
                    Icon(icon,color: Colors.red,)
                    : Icon(icon),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        text,
                        style: TextStyle(
                          fontSize: 17.0
                        )
                      ),
                    ),
                  ],
                ),
                
                Icon(Icons.arrow_right),
              ],
            ),
          ),
        ),
      ),
    );
  }
}