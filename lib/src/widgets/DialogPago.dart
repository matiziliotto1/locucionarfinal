import 'package:flutter/material.dart';

class DialogPago extends StatefulWidget {
  int moneda;

  DialogPago({@required this.moneda});

  @override
  _DialogPagoState createState() => _DialogPagoState();
}

class _DialogPagoState extends State<DialogPago> {
  List<Widget> opcionesPesos(BuildContext context) {
    return [
      Container(
        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width * 0.3,
        child: RaisedButton(
          color: Colors.white,
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacementNamed('/PagoPaypal');
          },
          child: Image.asset(
            "assets/images/PayPal-Logo.png",
            fit: BoxFit.cover,
          ),
        ),
      ),
      Container(
        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width * 0.3,
        child: RaisedButton(
          color: Colors.white,
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacementNamed('/PagoMercadoPago');
          },
          child: Image.asset("assets/images/MercadoPago-logoMano.png"),
        ),
      ),
    ];
  }

  List<Widget> opcionesDolares(BuildContext context) {
    return [
      Container(
        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width * 0.4,
        child: RaisedButton(
          color: Colors.white,
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacementNamed('/PagoPaypal');
          },
          child: Image.asset(
            "assets/images/PayPal-Logo.png",
            fit: BoxFit.cover,
          ),
        ),
      ),
    ];
  }

  @override
  AlertDialog build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    if (widget.moneda != null) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(8.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        title: Center(child: Text("Metodo de pago")),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children:
                    widget.moneda == 0 ? opcionesPesos(context) : opcionesDolares(context))
          ],
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("Cancelar"),
          )
        ],
      );
    } else {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        title: Text("Error"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  "Ocurrió un error, por favor intente mas tarde",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("Aceptar"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      );
    }
  }
}
