import 'package:url_launcher/url_launcher.dart';

Future<void> enviarWhatsApp() async{
    const url = 'https://wa.me/5403415031131';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'No se puede abrir $url';
    }
  } 