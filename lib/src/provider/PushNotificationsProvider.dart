import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:locucionar/utils/globals.dart' as globals;

class PushNotificationProvider{
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final _mensajesStreamController = StreamController<String>.broadcast();
  Stream<String> get mensajes => _mensajesStreamController.stream;  

  

  initNotifications(){

    //Pedir permisos
    _firebaseMessaging.requestNotificationPermissions();

    //Aca se obtiene el token del dispositivo que lo identifica
    _firebaseMessaging.getToken().then( (token) async {
      print(token);
      //Almaceno el token para luego utilizarlo si ingresa a la app.
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString("token", token);
    });

    _firebaseMessaging.configure(

      //Cuando la aplicacion esta abierta y en primer plano
      onMessage: (info) async {
        if(info.isNotEmpty){
          info['tipoNotificacion']="onMessage";
        }
        String argumento = 'no-data';
        if( Platform.isAndroid ){
          info['plataforma']="Android";
          argumento = json.encode(info) ?? 'no-data';
        }
        else{
          info['plataforma']="IOS";
          Map<String, dynamic> data = {'data':{
            'title':info['title'],
            'body':info['body'],
            'plataforma':info['plataforma']
          }};
          argumento = json.encode(data) ?? 'no-data-ios';
        }

        globals.notificaciones = true;
        _mensajesStreamController.sink.add(argumento);
      },

      //Cuando la aplicacion esta cerrada
      onLaunch: (info) async {
        info['tipoNotificacion']="onLaunch";

        globals.notificaciones = true;
      },

      //Cuando la aplicacion esta abierta y en segundo plano
      onResume: (info) async{
        info['tipoNotificacion']="onResume";

        globals.notificaciones = true;
      },
    );
  }

  dispose(){
    _mensajesStreamController?.close();
  }
}