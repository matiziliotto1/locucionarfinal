import 'dart:convert';

import 'package:locucionar/src/models/ItemsPendientes.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ItemsPendientesProvider{
  SharedPreferences sharedPreferences;

  Future<List<ItemPendiente>> getItemsPendientes() async{
    final url = "https://clientes.locucionar.com/app_clientes_ws/getItemsPendientesCliente.php";

    sharedPreferences = await SharedPreferences.getInstance();
    
    Map data= {
      'id': sharedPreferences.getInt("id_cliente").toString(),
    };
    var response = await http.post(url, body:data);
    
    var jsonData;

    if(response.statusCode == 200){
      jsonData = jsonDecode(response.body);

      final items = new Items.fromJsonList(jsonData['items']);

      return items.itemsPendientes;
    }
    else{
      //_error = "Ocurrio un error al conectarse con el servidor";
    }
  }
}