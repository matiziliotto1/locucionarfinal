import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:locucionar/src/models/NotificacionesEnviadas.dart';

class NotificacionesEnviadasProvider{
  SharedPreferences sharedPreferences;

  Future<List<NotificacionEnviada>> getNotificaciones() async{
    final url = "https://clientes.locucionar.com/app_clientes/API/getNotificacionesEnviadas.php";

    sharedPreferences = await SharedPreferences.getInstance();
    
    Map data= {
      'id_usuario': sharedPreferences.getInt("id_cliente").toString(),
    };

    //TODO:Deberia ser un get y enviarle headers:data
    var response = await http.post(url, body:data);
    
    var jsonData;

    if(response.statusCode == 200){
      jsonData = jsonDecode(response.body);

      final notificaciones_aux = new NotificacionesEnviadas.fromJsonList(jsonData['notificaciones']);

      return notificaciones_aux.notificacionesEnviadas;
    }
    else{
      //_error = "Ocurrio un error al conectarse con el servidor";
    }
  }
}