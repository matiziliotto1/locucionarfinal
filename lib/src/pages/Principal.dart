import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:locucionar/src/widgets/DialogPago.dart';

import 'package:locucionar/src/widgets/SingleChildScrollViewWithScrollbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'package:locucionar/src/provider/ItemsPendientesProvider.dart';
import 'package:locucionar/src/widgets/ListTitlePersonalizado.dart';
import 'package:locucionar/src/pages/SplashScreen.dart';
import 'package:locucionar/src/pages/Notificaciones.dart';
import 'package:locucionar/src/functions/EnviarWhatsApp.dart';

import 'package:locucionar/utils/globals.dart' as globals;

class Principal extends StatefulWidget {
  @override
  _PrincipalState createState() => _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  //Global key para el scaffold
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  ScrollController _scrollController = new ScrollController();
  ScrollController _controladorLista = new ScrollController();

  //Variables para los items pendientes
  List<DataRow> itemsPendiente;
  final itemPendienteProvider = new ItemsPendientesProvider();

  //Variable para los datos almacenados
  SharedPreferences sharedPreferences;

  //Variables para los datos del cliente
  int _idCliente;
  String _nombreCliente;
  String _emailCliente;

  //Variables para la informacion de la situacion del cliente
  String _error = "";
  int _moneda;
  String _simbolo = "";
  double _credito;
  double _deuda;
  String _ultimaAcreditacion;

  bool isBarraAlFinal = false;
  bool _descontandoSaldo = false;

  @override
  void initState() {
    _getClienteLogeado();
    _getSaldosCliente();
    _getUltimaAcreditacion();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      //aca tengo que desatar la animacion del raisedButton
      setState(() {
        this.isBarraAlFinal = true;
      });
    } else {
      if (this.isBarraAlFinal) {
        setState(() {
          this.isBarraAlFinal = false;
        });
      }
    }
  }

  _getClienteLogeado() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _idCliente = sharedPreferences.getInt("id_cliente");
      _nombreCliente = sharedPreferences.getString("nombre_cliente");
      _emailCliente = sharedPreferences.getString("email_cliente");
    });
  }

  _getSaldosCliente() async {
    sharedPreferences = await SharedPreferences.getInstance();

    Map data = {
      'id': _idCliente.toString(),
    };

    var response = await http.post(
        "https://clientes.locucionar.com/app_clientes_ws/getSaldosCliente.php",
        body: data);
    var jsonData;

    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);

      print(jsonData);

      if (jsonData['error'] == "") {
        setState(() {
          _moneda = jsonData['moneda'];
          _simbolo = jsonData['simbolo'];

          if (jsonData['credito'] == null) {
            _credito = 0.0;
          } else {
            _credito = jsonData['credito'] / 1;
          }

          if (jsonData['deuda'] == null) {
            _deuda = 0.0;
          } else {
            _deuda = jsonData['deuda'] / 1;
          }
        });
      } else {
        setState(() {
          _error = jsonData['error'];
        });
      }
    } else {
      _error = "Ocurrio un error al conectarse con el servidor";
    }

    itemPendienteProvider.getItemsPendientes().then((listaItems) {
      //UNA VEZ QUE TENGO TODA LA INFORMACION TENGO QUE SETEAR LA LISTA
      setState(() {
        this.itemsPendiente = <DataRow>[];
      });

      if (listaItems != null) {
        //Uso esta variable para no escribir lo mismo en todos los style de los text
        TextStyle itemsStyle = TextStyle(
          color: globals.itemsTextColor,
        );

        if (listaItems.isEmpty) {
          //CREAR LA ROW VACIA
          this.itemsPendiente.add(new DataRow(cells: [
                DataCell(
                    Text("-", textAlign: TextAlign.center, style: itemsStyle)),
                DataCell(
                    Text("-", textAlign: TextAlign.center, style: itemsStyle)),
                DataCell(
                    Text("-", textAlign: TextAlign.center, style: itemsStyle)),
                DataCell(
                    Text("-", textAlign: TextAlign.center, style: itemsStyle)),
              ]));
        } else {
          for (var item in listaItems) {
            this.itemsPendiente.add(new DataRow(cells: [
                  DataCell(Text(item.fecha, style: itemsStyle)),
                  DataCell(Text(item.concepto, style: itemsStyle)),
                  DataCell(Text(
                    item.importe,
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: globals.itemsTextColor,
                    ),
                  )),
                  _credito <
                          double.parse(item.importeNumero.replaceAll(",", "."))
                      ? DataCell(Text(
                          "Saldo insuficiente",
                          style: TextStyle(
                            color: Color.fromRGBO(255, 12, 12, 1),
                          ),
                        ))
                      : DataCell(
                          Text(
                            "Descontar de saldo",
                            style: TextStyle(
                              color: Color.fromRGBO(0, 161, 218, 1),
                            ),
                          ), onTap: () async {
                          setState(() {
                            _descontandoSaldo = true;
                          });

                          final url =
                              "https://clientes.locucionar.com/app_clientes_ws/descontarItemPendiente.php";
                          Map data = {
                            'id_item': item.id.toString(),
                            'id_cliente': _idCliente.toString(),
                            'moneda': _moneda.toString(),
                          };
                          var response = await http.post(url, body: data);

                          if (response.statusCode == 200) {
                            //Salio todo bien?
                            if (response.body == "1") {
                              showDialog(
                                context: context,
                                builder: (_) => AlertDialog(
                                  content: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.check_circle,
                                        color: Colors.green,
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Transacción exitosa",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: globals.infTextColor,
                                              fontSize: 20,
                                              fontWeight:
                                                  FontWeight.w700),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ).then((value) {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                    content: Container(
                                      child: Text.rich(
                                        TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: '¡Importante!',
                                              style: TextStyle(
                                                color: globals.infTextColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 20.0,
                                              ),
                                            ),
                                            TextSpan(
                                              text: '\n\nLa acreditación del pago puede demorar varios minutos en aparecer en su cuenta.',
                                              style: TextStyle(
                                                color: globals.infTextColor,
                                              )
                                            ),
                                          ]
                                        ),
                                      )
                                    ),
                                  )
                                );
                              }).then((onValue) {
                                _refresh();
                              });
                            } else {
                              //Si recibio un 0 es porque ocurrio un error en la transaccion.
                              showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        content: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.error,
                                              color: Colors.red,
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 8.0),
                                              child: Text(
                                                "Transacción errónea",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: globals.infTextColor,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ));
                            }
                            setState(() {
                              _descontandoSaldo = false;
                            });
                          } else {
                            _error =
                                "Ocurrio un error al conectarse con el servidor";
                          }
                        }),
                ]));
          }
        }
      }
    });
  }

  _getUltimaAcreditacion() async {
    final url =
        "https://clientes.locucionar.com/app_clientes_ws/getUltimaAcreditacionCliente.php";

    sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'id': sharedPreferences.getInt("id_cliente").toString(),
    };
    var response = await http.post(url, body: data);
    var jsonData;

    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);

      if (jsonData['error'] == "") {
        setState(() {
          _ultimaAcreditacion = jsonData['ultima_acreditacion'];
        });
      } else {
        setState(() {
          _error = jsonData['error'];
        });
      }
    } else {
      _error = "Ocurrio un error al conectarse con el servidor";
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color(0xFF1B3849),
        title: Text(
          "Hola " + _nombreCliente,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        leading: Stack(
          children: [
            Center(
              child: IconButton(
                icon: ImageIcon(
                  AssetImage("assets/icons/iconMenu.png"),
                ),
                onPressed: () => _scaffoldKey.currentState.openDrawer()
              ),
            ),
             globals.notificaciones ? new Positioned(
                right: 8,
                top: 8,
                child: new Container(
                  padding: EdgeInsets.all(2),
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 12,
                    minHeight: 12,
                  ),
                ),
              ) : new Container()

          ]
        ),
        centerTitle: true,
      ),
      drawer: _crearMenu(),
      body: RefreshIndicator(
        child: Stack(
          children: <Widget>[
            AnimatedOpacity(
              opacity: _descontandoSaldo == true ? 0.6 : 1.0,
              duration: const Duration(milliseconds: 200),
              child: ListView(
                physics: const AlwaysScrollableScrollPhysics(),
                controller: _controladorLista,
                children: <Widget>[
                  encabezado(width, height),
                  botonPago(width, height),
                  datos(width, height),
                  _crearListaItemsPendientes(width, height)
                ],
              ),
            ),
            AnimatedPositioned(
              duration: Duration(milliseconds: 400),
              bottom: isBarraAlFinal ? -70.0 : 30.0,
              right: isBarraAlFinal ? -70 : 10,
              child: FloatingActionButton(
                heroTag: "Abrir WhatsApp",
                backgroundColor: Colors.green,
                child: ImageIcon(AssetImage(
                  "assets/icons/iconWhatsapp.png",
                )),
                onPressed: enviarWhatsApp,
              ),
            ),
            _descontandoSaldo == true
                ? Opacity(
                    opacity: 1.0,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Color.fromRGBO(40, 56, 73, 1))),
                    ),
                  )
                : Container()
          ],
        ),
        onRefresh: _refresh,
        color: Colors.white,
        backgroundColor: Color.fromRGBO(40, 56, 73, 1),
      ),
    );
  }

  Future<void> _refresh() async {
    setState(() {
      this.itemsPendiente = null;

      //Variables para la informacion de la situacion del cliente
      _error = "";
      _moneda = null;
      _simbolo = "";
      _credito = null;
      _deuda = null;
      _ultimaAcreditacion = null;
    });

    _getSaldosCliente();
    _getUltimaAcreditacion();
  }

  Container encabezado(double width, double height) {
    return Container(
      //padding: EdgeInsets.symmetric(horizontal: width * 0.1),
      width: width,
      child: Stack(
        //fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(
            child: Image.asset(
              "assets/images/fondoPrincipal.jpg",
              fit: BoxFit.cover,
              color: Color(0xFF1B3849),
              colorBlendMode: BlendMode.modulate,
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(width * 0.10, 10.0, width * 0.10, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "Estado de Cuenta",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                SizedBox(height: 20.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Saldo pendiente: ",
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    Text(
                      _deuda != null
                          ? _simbolo + " " + _deuda.toString()
                          : _simbolo + " 0",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    )
                  ],
                ),
                Transform(
                  transform: Matrix4.skewX(-0.75),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Divider(
                      height: 10.0,
                      thickness: 6.0,
                      color: Colors.red[500],
                    ),
                  ),
                ),
                SizedBox(height: 15.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Saldo a favor: ",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    Text(
                      _credito != null
                          ? _simbolo + " " + _credito.toString()
                          : _simbolo + " 0",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    )
                  ],
                ),
                Transform(
                  transform: Matrix4.skewX(-0.75),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Divider(
                      height: 10.0,
                      thickness: 6.0,
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container datos(double width, double height) {
    return Container(
      child: Card(
        child: Column(
          children: _crearListaDeInformacion(width, height),
        ),
      ),
    );
  }

  Widget _crearListaItemsPendientes(double width, double height) {
    TextStyle cabecera = TextStyle(
        fontWeight: FontWeight.bold,
        color: globals.itemsTextColor,
        fontSize: width * 0.04);

    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: this.itemsPendiente == null
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Color.fromRGBO(40, 56, 73, 1))),
            )
          : Column(
              children: <Widget>[
                Text(
                  "Items Pendientes de Pago",
                  style: TextStyle(
                    fontSize: width * 0.045,
                    color: globals.itemsTextColor,
                  ),
                ),
                Container(
                  child: SingleChildScrollViewWithScrollbar(
                    scrollbarColor: Color.fromRGBO(15, 53, 71, 1),
                    scrollDirection: Axis.horizontal,
                    controller: this._scrollController,
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: globals.infTextColor),
                      child: DataTable(
                        columns: [
                          DataColumn(
                              label: Text(
                            "Fecha",
                            style: cabecera,
                          )),
                          DataColumn(
                              label: Text(
                            "Concepto",
                            style: cabecera,
                          )),
                          DataColumn(
                              label: Text("Importe", style: cabecera),
                              numeric: false),
                          DataColumn(
                              label: Text(
                            "Acciones",
                            style: cabecera,
                          )),
                        ],
                        rows: itemsPendiente,
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Container botonPago(double width, double height) {
    return Container(
      padding: EdgeInsets.fromLTRB(width * 0.3, 8.0, width * 0.3, 8.0),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        onPressed: () {
          seleccionarMetodo(context);
        },
        color: Colors.green,
        child: Text("Realizar pago",
            style: TextStyle(
              color: Colors.white,
              fontSize: width * 0.04,
              fontWeight: FontWeight.w700,
            )),
      ),
    );
  }

  Drawer _crearMenu() {
    return Drawer(
      child: Stack(children: [
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              DrawerHeader(
                child: Container(),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/fondo_menu.jpg",
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              CustomListTitle(
                  Icons.payment,
                  "Realizar pago",
                  () => {
                        Navigator.of(context).pop(),
                        this.seleccionarMetodo(context)
                      }),
              CustomListTitle(
                  Icons.notifications_none,
                  "Notificaciones",
                  () => {
                        Navigator.of(context).pop(),
                        Navigator.of(context).pushReplacementNamed('/Notificaciones'),
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => Notificaciones())),
                        //Navigator.of(context).pushNamed("/Notificaciones")
                      }),
              CustomListTitle(Icons.lock_outline, "Cerrar sesión", () {
                showDialog(
                      context: context,
                      child: AlertDialog(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        title: Text('Estas seguro que deseas cerrar sesión?'),
                        actions: <Widget>[
                          RaisedButton(
                            onPressed: () async {
                              //Almaceno el token, ya que es necesario tenerlo guardado, borro todas las preferencias y vuelvo a setear el token.
                              var token = sharedPreferences.getString('token');
                              var idUsuario =
                                  sharedPreferences.getInt("id_cliente");
                              sharedPreferences.clear();
                              sharedPreferences.setString('token', token);

                              var url = '';
                              Map data;

                              //Obtengo el token guardado del dispositivo y lo almaceno en la base de datos.
                              //Tener en cuenta que el usuario puede tener mas de un dispositivo.
                              url =
                                  'https://clientes.locucionar.com/app_clientes/API/deleteToken.php';
                              data = {
                                'token': token,
                                'id_usuario': idUsuario.toString(),
                              };

                              await http.post(url, body: data);
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (context) => SplashScreen()),
                                  (Route<dynamic> route) => false);
                            },
                            child: Text(
                              'Si',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Color.fromRGBO(41, 171, 226, 1),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Text('No'),
                            color: Colors.red.shade400,
                          ),
                        ],
                      ),
                    ) ??
                    false;
              }),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: Center(
                child: FloatingActionButton(
                  heroTag: "Cerrar menu",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(Icons.close),
                  backgroundColor: Colors.red.shade400,
                ),
              ),
            ),
          ],
        ),
      ]),
    );
  }

  List<Widget> _crearListaDeInformacion(double width, double height) {
    final List<Widget> itemsInformacion = <Widget>[];

    if (_credito != null && _credito != 0.0) {
      itemsInformacion.add(ListTile(
          leading: Icon(Icons.info_outline),
          title: Text.rich(
            TextSpan(children: <TextSpan>[
              TextSpan(
                text:
                    'Posee saldo a favor, puede utilizarlo para abonar productos pendientes haciendo ',
                style: TextStyle(
                    fontSize: width * 0.04, color: globals.infTextColor),
              ),
              TextSpan(
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      this._controladorLista.animateTo(
                          this._controladorLista.position.maxScrollExtent,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.ease);
                      //ESTO ES OPCIONAL, SI NO GUSTA LO SACAMOS
                      this._scrollController.animateTo(
                          this._scrollController.position.maxScrollExtent,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear);
                    },
                  text: 'click aquí',
                  style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.green,
                      fontWeight: FontWeight.w700)),
            ]),
          )));
      itemsInformacion.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: width * 0.08),
        child: Divider(
          height: 10.0,
          thickness: 3.0,
          color: Colors.blue[900],
        ),
      ));
    }

    if (_deuda != null && _deuda != 0.0) {
      double deudaAux = double.parse(_deuda.toString().substring(1));
      double creditoAux = 0.0;
      if (_credito != null) {
        creditoAux = _credito;
      }
      if (deudaAux > _credito) {
        double resta = _deuda;
        resta = deudaAux - creditoAux;
        resta = double.parse("-" + resta.toString());

        itemsInformacion.add(ListTile(
            leading: Icon(Icons.info_outline),
            title: Text.rich(
              TextSpan(children: <TextSpan>[
                TextSpan(
                  text: 'Resta abonar un total de ',
                  style: TextStyle(
                      fontSize: width * 0.04, color: globals.infTextColor),
                ),
                TextSpan(
                    text: _simbolo + resta.toString(),
                    style: TextStyle(
                      fontSize: width * 0.04,
                      fontWeight: FontWeight.w700,
                    )),
                TextSpan(
                    text: ' para quedar sin ',
                    style: TextStyle(
                        fontSize: width * 0.04, color: globals.infTextColor)),
                TextSpan(
                    text: 'Saldo Pendiente.',
                    style: TextStyle(
                      fontSize: width * 0.04,
                      color: Colors.red,
                      fontWeight: FontWeight.w700,
                    )),
              ]),
            )));
        itemsInformacion.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.08),
          child: Divider(
            height: 10.0,
            thickness: 3.0,
            color: Colors.blue[900],
          ),
        ));
      }
    }

    if (_ultimaAcreditacion != null) {
      itemsInformacion.add(ListTile(
        leading: Icon(Icons.info_outline),
        title: Text(
          "Última acreditación: " + _ultimaAcreditacion,
          style: TextStyle(fontSize: width * 0.04, color: globals.infTextColor),
        ),
      ));
      itemsInformacion.add(
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.08),
          child: Divider(
            height: 10.0,
            thickness: 3.0,
            color: Colors.blue[900],
          ),
        ),
      );
    }
    return itemsInformacion;
  }

  void seleccionarMetodo(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return DialogPago(moneda: this._moneda);
        });
  }
}
