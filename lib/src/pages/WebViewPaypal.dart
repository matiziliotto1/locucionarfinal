import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WebViewPaypal extends StatefulWidget {
  @override
  _WebViewPaypalState createState() => _WebViewPaypalState();
}

class _WebViewPaypalState extends State<WebViewPaypal> {
  final flutterWebViewPlugin = new FlutterWebviewPlugin();
  bool _isLoadingPage = false;

  SharedPreferences sharedPreferences;
  int _idCliente;

  @override
  void initState() {
    super.initState();
    inicializarShared();

    //SOLUCION NUEVA, TIENE SENTIDO
    flutterWebViewPlugin.onDestroy.listen((_) {
        Navigator.of(context).pushReplacementNamed("/Principal");
    });

    flutterWebViewPlugin.onProgressChanged.listen((valor) {
      if (valor == 1.0) {
        setState(() {
          _isLoadingPage = false;
        });
      } else {
        if (!_isLoadingPage) {
          //poner el loading
          setState(() {
            _isLoadingPage = true;
          });
        }
      }
    });
    flutterWebViewPlugin.onUrlChanged.listen((url) {
      verificarUrl(url);
    });
  }

  @override
  Widget build(BuildContext context) {
    //cuando cambia de link
    flutterWebViewPlugin.onUrlChanged.listen((url) {
      verificarUrl(url);
    });

    //Funcion para que no se cierre de forma extraña
    flutterWebViewPlugin.onDestroy.listen((data) {
      flutterWebViewPlugin.dispose();
    });

    flutterWebViewPlugin.onProgressChanged.listen((valor) {
      if (valor == 1.0) {
        setState(() {
          _isLoadingPage = false;
        });
      } else {
        if (!_isLoadingPage) {
          //poner el loading
          setState(() {
            _isLoadingPage = true;
          });
        }
      }
    });

    return this._idCliente == null ? Center(child:CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Color.fromRGBO(41, 171, 226, 1)))) : 
    WebviewScaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF1B3849),
        title: Text(
          "Pago con: Paypal",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        leading: new IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => {
              Navigator.of(context).pushReplacementNamed("/Principal"),
            }
          ),
        centerTitle: true,
      ),
      url:
          'https://clientes.locucionar.com/app_clientes/Paypal/index.php?id_cliente=' +
              _idCliente.toString(),
      withJavascript: true, //CREO QUE ESTO HABILITA JS
      initialChild: _isLoadingPage
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Color.fromRGBO(41, 171, 226, 1))))
          : Container(),
    );
  }

  void verificarUrl(String url) async {
    if (url ==
            'https://clientes.locucionar.com/app_clientes/Paypal/transaccionExitosa.php#' ||
        url ==
            'https://clientes.locucionar.com/app_clientes/Paypal/transaccionFallida.php#') {
      flutterWebViewPlugin.close();
    }
  }

  Future<void> inicializarShared() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _idCliente = sharedPreferences.getInt('id_cliente');
    });
  }
}
