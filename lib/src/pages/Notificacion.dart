import 'package:flutter/material.dart';

import 'package:locucionar/utils/globals.dart' as globals;

class Notificacion extends StatelessWidget {
  String titulo;
  String cuerpo;

  Notificacion(this.titulo,this.cuerpo);

  @override
  Widget build(BuildContext context) {

    //Si se crea la notificacion a partir de una local notification
    String args = ModalRoute.of(context).settings.arguments;
    //Si los argumentos son nulos, es porque NO se creo a partir de una local notification
    //Si son diferente de null, entonces seteo las titulo y cuerpo, con lo recibido como argumento.
    if(args != null){
      var corte = args.indexOf("|");
      this.titulo = args.substring(0,corte);
      this.cuerpo = args.substring(corte+1,args.length);
    }
    
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF1B3849),
        title: Text(
          "Ver notificación",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        leading: Container(),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Color.fromRGBO(180, 210, 226, 0.15),
        child: Stack(
          children: [
            Container(
              child: Column(
                children: [
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:20.0,bottom: 15.0),
                        child: Card(
                          color: Color.fromRGBO(61, 95, 114, 1),
                          child: ListTile(
                            title: Text(
                              this.titulo,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 21.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          title: Text(
                            this.cuerpo,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: globals.itemsTextColor,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ]
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Align(
                    child: SizedBox(
                      width: 200.0,
                      height: 50.0,
                      child: FlatButton.icon(
                        color: Color.fromRGBO(0, 15, 32, 1),
                        onPressed: (){
                          Navigator.of(context).pushReplacementNamed("/Principal");
                        },
                        label: Text(
                          "Regresar",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    alignment: Alignment.bottomCenter,
                  ),
                ],
              ),
            ),
          ]
        ),
      ),
    );
  }
}