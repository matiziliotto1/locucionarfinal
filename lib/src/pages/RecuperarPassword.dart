import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RecuperarPassword extends StatefulWidget {
  RecuperarPassword({Key key}) : super(key: key);

  @override
  _RecuperarPasswordState createState() => _RecuperarPasswordState();
}

class _RecuperarPasswordState extends State<RecuperarPassword> {
  final _formKey = GlobalKey<FormState>();
  Color _colorPrincipal = Color.fromRGBO(41, 171, 226, 1);
  TextEditingController _controladorEmail = new TextEditingController();
  String _email;

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/fondo_computadora.jpg"),
              fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(
              horizontal: 40.0,
              vertical: height * 0.05,
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: height * 0.025),
                  child: Container(
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/logo.png",
                            scale: 3,
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.only(top: 50.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //SizedBox(height: height * 0.15),
                      Text(
                          "Ingrese el correo electrónico con el cual se registró.\nLe enviaremos una nueva contraseña.",
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(color: Colors.white, fontSize: 20.0)),
                      SizedBox(height: height * 0.10),
                      _crearFormulario(),
                      _crearBotones(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearFormulario() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _crearInputUsuario(),
        ],
      ),
    );
  }

  Widget _crearInputUsuario() {
    return TextFormField(
      controller: _controladorEmail,
      style: TextStyle(
        color: Color.fromRGBO(240, 240, 240, 1),
      ),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
        )),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
          width: 2.5,
        )),
        hintText: 'Correo electrónico',
        hintStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
        prefixIcon: Icon(
          Icons.mail,
          color: _colorPrincipal,
          size: 30.0,
        ),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Debe ingresar un email';
        }
      },
    );
  }

  Widget _crearBotones() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 20.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          RaisedButton(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
            elevation: 5.0,
            onPressed: () {
              //Si el formulario fue validado
              if (_formKey.currentState.validate()) {
                //enviar consulta al api y mostrar un dialog o algo similar
                this._email = this._controladorEmail.text;
                consultarWebService(this._email);
              }
            },
            color: _colorPrincipal,
            child: Text(
              "Enviar",
              style: TextStyle(
                color: Color.fromRGBO(240, 240, 240, 1),
                fontSize: 25.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
            elevation: 5.0,
            color: _colorPrincipal,
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              "Volver",
              style: TextStyle(
                color: Color.fromRGBO(240, 240, 240, 1),
                fontSize: 25.0,
                fontWeight: FontWeight.w300,
              ),
            ),
          )
        ],
      ),
    );
  }

  consultarWebService(String email) async {
    //hacer el http request,
    Map<String, String> data = {"email": email};

    Map<String, dynamic> datos = {};

    var respuesta = await http.post(
        "https://clientes.locucionar.com/app_clientes_ws/recuperarClave.php",
        body: data);
    //ESTO ME CONTESTA UN 'error' : ''  cuando todo salio bien
    //cuando todo esta mal me devuelve 'error' : 'Ocurrio un error'
    if (respuesta.statusCode == 200) {
      datos = json.decode(respuesta.body);
    } else {
      datos['error'] = 'Ocurrió un error, intente más tarde';
    }

    //mostrar dialog que es correcto, al aceptar que vaya para el login.
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.all(5.0),
            contentTextStyle: TextStyle(fontSize: 18.0, color: Colors.black),
            title: datos['error'] == '' //significa que todo salio bien
                ? Center(
                    child: Icon(
                    Icons.beenhere,
                    size: 30.0,
                    color: _colorPrincipal,
                  ))
                : Center(
                    child: Icon(
                    Icons.error,
                    color: Colors.red,
                    size: 30.0,
                  )),
            content: datos['error'] == ''
                ? Text(
                    "Consulte su casilla de correo para restaurar su contraseña",
                    textAlign: TextAlign.center,
                  )
                : Text(
                    "Ocurrió un error, intente nuevamente",
                    textAlign: TextAlign.center,
                  ),
            actions: <Widget>[
              Center(
                child: FlatButton(
                    onPressed: () {
                      if (datos['error'] == '') {
                        Navigator.of(context).pushReplacementNamed('/Login');
                      } else {
                        this._controladorEmail.text=this._email;
                        Navigator.of(context)
                            .pop(); //se queda en la pagina de recuperacion, hace pop del Dialog
                      }
                    },
                    child: Text("Aceptar")),
              )
            ],
          );
        });
  }
}
