import 'package:flutter/material.dart';
import 'package:locucionar/src/pages/Carrusel.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget{
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences sharedPreferences;
  String _nextPage;

  @override
  void initState() {
    super.initState();

    startTime();
  }

  startTime() async {
    var duration = new Duration(seconds: 1);
    _checkNextPage();
    return new Timer(duration, route);
  }

  void _checkNextPage() async{
    sharedPreferences = await SharedPreferences.getInstance();

    if(sharedPreferences.getString("logeado") == "1"){
      setState(() {
        _nextPage = "/Principal";
      });
    }
    else{
      setState(() {
        _nextPage = "/Login";
      });
    }
  }

  route(){
    Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => Carrusel(text: _nextPage,),));
  }

  route2(){
    Navigator.of(context).pushReplacementNamed("/Notificacion",arguments: sharedPreferences.getString("Notificacion"));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
       decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/fondo_computadora.jpg"), 
                fit: BoxFit.cover
            )
       ),

       child: Scaffold(
         backgroundColor: Colors.transparent,
         body: Stack(
           fit: StackFit.expand,
           children: <Widget>[
             Column(
               mainAxisAlignment: MainAxisAlignment.start,
               children: <Widget>[
                 Expanded(
                   flex: 5,
                   child: Container(
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                        Image.asset(
                          "assets/images/logo.png",
                          scale: 3,
                        ),
                       ],
                     ),
                   ),
                 ),
               ],
             ),
             Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Color.fromRGBO(41, 171, 226, 1)
                    ),
                  ),
                ),
              ],
            ),
           ],
         ), 
       ),
    );
  }
}