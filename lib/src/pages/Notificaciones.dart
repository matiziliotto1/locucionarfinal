import 'package:flutter/material.dart';

import 'package:locucionar/src/provider/NotificacionesEnviadasProvider.dart';
import 'package:locucionar/src/widgets/CardNotificacion.dart';
import 'package:locucionar/utils/globals.dart' as globals;

class Notificaciones extends StatefulWidget {
  @override
  _NotificacionesState createState() => _NotificacionesState();
}

class _NotificacionesState extends State<Notificaciones> {
  //Variables para las notificaciones enviadas
  List<CardNotificacion> notificacionesEnviadas;
  final notificacionesEnviadasprovider = new NotificacionesEnviadasProvider();

  @override
  void initState() { 
    globals.notificaciones = false;
    notificacionesEnviadasprovider.getNotificaciones().then((listaNotif){
      //Una vez recibida la lista de notificaciones
      this.notificacionesEnviadas = <CardNotificacion>[];
      if(listaNotif != null){
        if(listaNotif.isEmpty){
          this.notificacionesEnviadas.add(
              CardNotificacion("-1","Usted no tiene nuevas notificaciones.", "")
            );

          // this.notificacionesEnviadas.add();
        }
        else{
          for (var notif in listaNotif){
            this.notificacionesEnviadas.add(
              CardNotificacion(notif.idNotificacion,notif.titulo, notif.cuerpo)
            );
          }
        }
         setState(() {
            this.notificacionesEnviadas;
          });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF1B3849),
        title: Text(
          "Notificaciones",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        leading: new IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => {
              Navigator.of(context).pushReplacementNamed("/Principal"),
            }
          ),
        centerTitle: true,
      ),
      body: Container(
        color: Color.fromRGBO(180, 210, 226, 0.15),
        child: ListView(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
                this.notificacionesEnviadas == null ?
                [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top:25.0),
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color.fromRGBO(40, 56, 73, 1))),
                  ),
                )]
                : this.notificacionesEnviadas
            ),
          ],
        ),
      ),
    );
  }
}