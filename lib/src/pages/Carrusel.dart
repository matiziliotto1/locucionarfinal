import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:locucionar/src/widgets/pestaniaPresentacion.dart';

class Carrusel extends StatefulWidget {
  final String text;

  Carrusel({@required this.text});

  @override
  _CarruselState createState() => _CarruselState(nextPage: text);
}

class _CarruselState extends State<Carrusel> {
  final String nextPage;

  _CarruselState({@required this.nextPage});

  final List<Widget> pantallasPresentacion = <Widget>[];

  PageController _controller;
  int currentPageValue = 0;

  @override
  void initState() {
    super.initState();
    getFotos().then((resultado) {
      if (resultado['error'] == null) {
        for (var item in resultado['items_carrousel']) {
          this.pantallasPresentacion.add(PestaniaPresentacion(
                imagen: item['imagen'],
                titulo: item['titulo_imagen'],
                texto: item['texto_imagen'],
              ));
        }
      } else {
        Navigator.of(context).pushReplacementNamed('/Login');
      }
      setState(() {});
    });
  }

  Future<Map<String, dynamic>> getFotos() async {
    var respuesta = await http.get(
        "https://clientes.locucionar.com/app_clientes/API/getInfoCarrousel.php");
    if (respuesta.statusCode == 200) {
      //error del json
      return jsonDecode(respuesta.body);
    } else {
      //Error 500 o no se conecto a la api
      return {"error": true};
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: height * 0.9,
                child: PageView.builder(
                    itemCount: this.pantallasPresentacion.length,
                    physics: ClampingScrollPhysics(),
                    onPageChanged: (int page) {
                      getChangedPage(page);
                    },
                    controller: _controller,
                    itemBuilder: (context, index) {
                      if (this.pantallasPresentacion.length == 0) {
                        return Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Color.fromRGBO(41, 171, 226, 1)),
                          ),
                        );
                      } else {
                        return pantallasPresentacion[index];
                      }
                    }),
              ),
              //ACA VA EL BOTON CON EL 10% DE LA PANTALLA
              Container(
                width: width,
                height: height * 0.1,
                child: FlatButton(
                    color: Color.fromRGBO(127, 195, 72, 1),
                    onPressed: () {
                      if (nextPage == "/Principal") {
                        Navigator.pushReplacementNamed(context, nextPage);
                      } else {
                        Navigator.of(context).pushNamed(nextPage);
                      }
                    },
                    child: Text("Ingresar",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: width * 0.05,
                          fontWeight: FontWeight.w400,
                        ))),
              )
            ],
          ),
          //ACA VAN LO 3 PUNTOS DEL CARRUSEL
          Positioned(
            child: Container(
              margin: EdgeInsets.only(top: height * 0.85),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < this.pantallasPresentacion.length; i++)
                    if (i == currentPageValue) ...[circleBar(true)] else
                      circleBar(false),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void getChangedPage(int page) {
    currentPageValue = page;
    setState(() {});
  }

  Widget circleBar(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 5),
      height: isActive ? 10 : 8,
      width: isActive ? 10 : 8,
      decoration: BoxDecoration(
          color: isActive
              ? Color.fromRGBO(127, 195, 72, 1)
              : Color.fromRGBO(57, 88, 33, 1),
          borderRadius: BorderRadius.all(Radius.circular(12))),
    );
  }
}
