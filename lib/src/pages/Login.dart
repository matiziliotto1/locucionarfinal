import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:locucionar/src/pages/Principal.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Color _colorPrincipal = Color.fromRGBO(41, 171, 226, 1);

  final FocusNode _correo = FocusNode();
  final FocusNode _contrasenia = FocusNode();

  TextEditingController _usuarioController=new TextEditingController();

  String _usuario;
  String _password;

  bool _recordar = false;
  bool _autenticando = false;
  bool _loginError = false;
  String _error = "";

  //Para validar los datos ingresados
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    this._usuarioController.dispose();
    super.dispose();
  }
  

  @override
  Widget build(BuildContext context) {
    //final double width=MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/fondo_computadora.jpg"),
              fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: _autenticando
            ? Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Color.fromRGBO(41, 171, 226, 1))),
              )
            : GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: Container(
                  height: double.infinity,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding: EdgeInsets.symmetric(
                      horizontal: 40.0,
                      vertical: height * 0.05,
                    ),
                    child: Column(
                      children: <Widget>[
                        Stack(children: [
                          Padding(
                            padding: EdgeInsets.only(top: height * 0.025),
                            child: Container(
                                width: double.infinity,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      "assets/images/logo.png",
                                      scale: 3,
                                    ),
                                  ],
                                )),
                          ),
                        ]),
                        Padding(
                          padding: EdgeInsets.only(top: height * 0.1),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: height * 0.15),
                                _crearFormulario(),
                                SizedBox(height: height * 0.02),
                                _crearBotonIngresar(),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: height * 0.2),
                          child: Container(
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                _crearOlvidasteTuPassword(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  Widget _crearFormulario() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _crearInputUsuario(),
          _crearInputPassword(),
        ],
      ),
    );
  }

  Widget _crearInputUsuario() {
    return TextFormField(
      focusNode: _correo,
      controller: _usuarioController,
      onFieldSubmitted: (data){
        cambiarFocoCampo(context, _correo, _contrasenia);
      },
      style: TextStyle(
        color: Color.fromRGBO(240, 240, 240, 1),
      ),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
        )),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
          width: 2.5,
        )),
        hintText: 'Usuario',
        hintStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
        prefixIcon: Icon(
          Icons.person,
          color: _colorPrincipal,
          size: 30.0,
        ),
        errorText: _loginError ? '' : null,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Debe ingresar el usuario.';
        }
      },
    );
  }

  Widget _crearInputPassword() {
    return TextFormField(
      focusNode: _contrasenia,
      style: TextStyle(
        color: Color.fromRGBO(240, 240, 240, 1),
      ),
      obscureText: true,
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
        )),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
          color: _colorPrincipal,
          width: 2.5,
        )),
        hintText: 'Contraseña',
        hintStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
        prefixIcon: Icon(
          Icons.lock,
          color: _colorPrincipal,
          size: 30.0,
        ),
        errorText: _loginError ? _error : null,
      ),
      onChanged: (valor) {
        _password = valor;
      },
      validator: (value) {
        if (value.isEmpty) {
          return 'Debe ingresar la contraseña.';
        }
      },
    );
  }

  Widget _crearBotonIngresar() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
      ),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
        elevation: 5.0,
        onPressed: () {
          //Si el formulario fue validado
          if (_formKey.currentState.validate()) {
            setState(() {
              _autenticando = true;
            });
            this._usuario=this._usuarioController.text;
            autenticacion(_usuario, _password);
          }
        },
        color: _colorPrincipal,
        child: Text(
          "Acceder",
          style: TextStyle(
            color: Color.fromRGBO(240, 240, 240, 1),
            fontSize: 25.0,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  Widget _crearOlvidasteTuPassword() {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, '/RecuperarPassword');
      },
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
                text: "¿Olvidaste tu contraseña?",
                style: TextStyle(
                  color: Color.fromRGBO(240, 240, 240, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 18.0,
                )),
          ],
        ),
      ),
    );
  }

  autenticacion(String usuario, String password) async {
    Map data = {'user': usuario, 'pass': password};

    var jsonData;
    SharedPreferences sharedpreferences = await SharedPreferences.getInstance();

    var response = await http.post(
        "https://clientes.locucionar.com/app_clientes_ws/ingresarCliente.php",
        body: data);

    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);

      if (jsonData['error'] == "") {
        String token = "";
        var url = '';
        Map data;
        setState(() {
          sharedpreferences.setString("logeado", "1");
          sharedpreferences.setInt("id_cliente", jsonData['id']);
          sharedpreferences.setString("nombre_cliente", jsonData['nombre']);
          sharedpreferences.setString("email_cliente", jsonData['email']);
        });

        //Obtengo el token guardado del dispositivo y lo almaceno en la base de datos.
        //Tener en cuenta que el usuario puede tener mas de un dispositivo.
        token = sharedpreferences.getString('token');
        url = 'https://clientes.locucionar.com/app_clientes/API/setToken.php';
        data = {
          'token': token,
          'id_usuario': jsonData['id'].toString(),
        };

        await http.post(url, body: data);

        _autenticando = false;
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Principal()),
            (Route<dynamic> route) => false);
      } else {
        _autenticando = false;
        setState(() {
          _error = jsonData['error'];
          _loginError = true;
          this._usuarioController.text=this._usuario;
        });
      }
    } else {
      _autenticando = false;
      _loginError = true;
      _error = "Ocurrio un error al conectarse con el servidor";
      this._usuarioController.text=this._usuario;

    }
  }

  void cambiarFocoCampo(
      BuildContext context, FocusNode focoActual, FocusNode focoSiguiente) {
    focoActual.unfocus();
    FocusScope.of(context).requestFocus(focoSiguiente);
  }
}
